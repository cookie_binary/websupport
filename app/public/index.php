<?php
/**
 * WebSupport simple DNS administration
 * @author Martin Osusky
 */

// prepare simple autoloader
include_once "../src/autoloader.php";

// boot app
include_once "../src/bootstrap.php";
