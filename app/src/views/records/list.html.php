<?php
/** @var string $domainName */
/** @var array $records */
?>
<div class="row">
    <div class="col-md">
        <h2>
            DNS records
        </h2>
    </div>
    <div class="col-md" style="text-align: right;">
        <a href="/" class="btn btn-outline-primary">
            Dashboard
        </a>
        <a href="/record/edit?domainName=<?= $domainName ?>" class="btn btn-success">
            Add new DNS record
        </a>
    </div>
</div>

<?php if ($message = param("message")) { ?>
    <div class="alert alert-success <?= $message == 'deleting_successful' ? 'alert-danger' : '' ?>" role="alert">
        <?= match ($message) {
            'saved_successful' => 'DNS record was saved successfully.',
            'deleting_successful' => 'DNS record was deleted successfully.',
        } ?>
    </div>
    <script>
        setTimeout(() => {
            $("[role=alert]").fadeOut()
        }, 5000)
    </script>
<?php } ?>

<table class="table table-hover">
    <thead class="thead-dark">
    <tr>
        <th scope="col">ID</th>
        <th>Type</th>
        <th>Name</th>
        <th>Content</th>
        <th>TTL</th>
        <th>Note</th>
        <th style="text-align: right">Actions</th>
    </tr>
    </thead>

    <?php foreach ($records as $record) { ?>
        <tr>
            <td><?= $record->id ?></td>
            <td><?= $record->type ?></td>
            <td><?= $record->name ?></td>
            <td><?= $record->content ?></td>
            <td><?= $record->ttl ?></td>
            <td><?= $record->note ?></td>
            <td style="text-align: right">
                <a href="/record/edit?domainName=<?= $domainName ?>&recordId=<?= $record->id ?>"
                   class="btn btn-outline-primary btn-sm">
                    &#9998; Edit</a>

                <a href="/record/delete?domainName=<?= $domainName ?>&recordId=<?= $record->id ?>"
                   class="delete btn btn-outline-danger btn-sm">
                    &#10005; Delete</a>
            </td>
        </tr>
    <?php } ?>
</table>

<style>

</style>

<script>
    $(document).ready(() => {
        $(".delete").click(function (event) {
            if (!confirm("Are you sure?")) {
                event.preventDefault()
            }
        })
    })
</script>