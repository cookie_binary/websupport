<?php

use \Src\Controllers\Records;

/** @var array $data */
/** @var string $domainName */
/** @var array $errors */
/** @var int $newRecord */
?>

<div class="row">
    <div class="col-md">
        <?php if ($newRecord) { ?>
            <h2>Create DNS record</h2>
        <?php } else { ?>
            <h2>Edit DNS record</h2>
        <? } ?>
    </div>
    <div class="col-md" style="text-align: right;" id="exit-buttons">
        <a href="/" class="btn btn-outline-primary">
            Dashboard
        </a>
        <a href="/record/list?domainName=<?= $domainName ?>" class="btn btn-outline-success">
            Cancel editing
        </a>
    </div>
</div>

<script>
    $(document).ready(() => {
        $("#exit-buttons a").click(function (event) {
            if (!confirm("Are you sure?")) {
                event.preventDefault()
            }
        })
    })
</script>

<div class="container">
    <div class="row">
        <div class="col">
        </div>
        <div class="col-6">

            <form method="POST" action="/record/store" class="mb-3">
                <input type="hidden" name="domainName" value="<?= $domainName ?>">
                <input type="hidden" name="recordId" value="<?= @$data['id'] ?>">

                <label class="form-label">
                    Record type </label>
                <?php if ($newRecord) { ?>
                    <select name="type" <?= @$data['id'] ? "readonly" : "" ?> class="form-control">
                        <?php foreach (Records::RECORD_TYPES as $recordType => $fields) { ?>
                            <option value="<?= $recordType ?>" <?= $recordType == @$data['type'] ? "selected" : "" ?>>
                                <?= $recordType ?></option>
                        <? } ?>
                    </select>
                <?php } else { ?>
                    <input name="type" value="<?= @$data['type'] ?>" readonly class="form-control">
                <?php } ?>

                <div class="mb-3">
                    <?php foreach (Records::RECORD_TYPES as $recordType => $fields) { ?>
                        <div id="form-<?= $recordType ?>" style="display: none" class="type-fields">
                            <?php foreach ($fields as $name => $type) { ?>
                                <div class="mb-3">
                                    <label class="form-label">
                                        <?= ucfirst($name) ?>   </label>
                                    <input type="<?= $type ?>" name="<?= $name ?>" value="<?= @$data[$name] ?>"
                                           class="form-control">
                                    <?php
                                    if (@$errors[$name]) { ?>
                                        <div class="alert alert-danger" role="alert">
                                            <?= implode("<br>", $errors[$name]) ?></div>
                                    <?php } ?>

                                </div>
                            <? } ?>
                        </div>
                    <? } ?>
                </div>

                <button class="btn btn-primary" id="save">
                    <?php if ($newRecord) { ?>
                        Create DNS record
                    <?php } else { ?>
                        Save DNS record
                    <? } ?>
                </button>

            </form>

        </div>
        <div class="col-sm">
        </div>
    </div>
</div>

<script>
    const changeRecordType = () => {
        let selectedRecordType = $("[name=type]").val()
        $(".type-fields").hide()
        $("#form-" + selectedRecordType).show()
    }

    $(document).ready(changeRecordType)
    $("select[name=type]").change(changeRecordType)

    $("#save").click(() => {
        // prepare data before send
        let selectedRecordType = $("[name=type]").val()
        $(".type-fields:not(#form-" + selectedRecordType + ")").remove();
    })
</script>



