<h2>Dashboard</h2>
<h3>
    List of your domains
</h3>
<div class="row">
    <div class="col-2">
    </div>
    <div class="col-7">
        <table class="table col">
            <thead class="thead thead-dark">
            <tr>
                <th>ID</th>
                <th>Domain Name</th>
                <th style="text-align: right">Action</th>
            </tr>
            </thead>
            <?php /** @var array $domains */
            foreach ($domains as $domain) { ?>
                <tr>
                    <td><?= $domain->id ?></td>
                    <td>
                        <a href="/record/list?domainName=<?= $domain->name ?>">
                            <?= $domain->name ?>
                        </a>
                    </td>
                    <td style="text-align: right">
                        <a href="/record/list?domainName=<?= $domain->name ?>" class="btn btn-outline-primary btn-sm">
                            &#9998; Manage</a>
                    </td>

                </tr>
            <?php } ?>
        </table>
    </div>

</div>