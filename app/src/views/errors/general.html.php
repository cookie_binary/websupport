<h3>
    An unexpected error has occurred</h3>
<div>
    Back to <a href="/">Dashboard</a>?
</div>
<?php if (getenv("DEBUG")) { ?>
    <div>
        <h3>Exception details</h3>
        <p>Note: This exception / error details were only displayed in debug mode.</p>
        <?php /** @var mixed $exception */
        dd($exception); ?></div>
<?php } ?>