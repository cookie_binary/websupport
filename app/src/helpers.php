<?php

/**
 * Global Helper functions here
 *
 * @author Martin Osusky
 */

use JetBrains\PhpStorm\NoReturn;
use JetBrains\PhpStorm\Pure;
use Src\Classes\Request;

if (!function_exists('dd')) {
    /**
     * Dump and Die
     * Inspired by Laravel
     * @param mixed ...$vars
     */
    #[NoReturn] function dd(...$vars)
    {
        foreach ($vars as $v) {
            echo "<pre style='background: #021522; color: #c6c6c6; padding: 10px; border-radius: 10px'>";
            print_r($v);
            echo "</pre>";
        }

        exit(1);
    }
}

if (!function_exists('param')) {
    /**
     * Returns parameter from GET or POST form request
     * @param $paramName
     * @return array|string|null
     */
    #[Pure] function param($paramName): array|string|null
    {
        return @$_POST[$paramName] ?? @$_GET[$paramName];
    }
}

if (!function_exists('redirect')) {
    /**
     * Simple redirection
     * @param string $url
     */
    #[NoReturn] function redirect(string $url)
    {
        header("Location: $url");
        exit;
    }
}