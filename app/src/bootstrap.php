<?php
/**
 * Boostrap loader
 * Loads and init everything 🌍
 *
 * @author Martin Osusky
 */

use Src\Classes\Env;
use Src\Classes\Request;
use Src\Classes\Router;
use Src\Classes\View;

// session start
session_start();

// include helpers
include_once "helpers.php";

// load environment variables
(new Env(__DIR__ . '/../.env'))->load();

// setup request calls
$request = new Request(getenv('API_URL'), getenv('API_KEY'), getenv('API_SECRET'), getenv('API_VERSION'));

// setup views
$view = new View("src/views/", "layout/main");

// setup router
$router = new Router(routes: include 'routes.php', request: $request, view: $view);

// resolve controller call and error processing
try {
    // process action and get response
    $response = $router->resolve();
} catch (Exception $exception) {
    // or error has occurred
    $response = $view->render("errors/general", compact('exception'));
}

// general output
echo $response;
