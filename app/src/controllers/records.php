<?php

namespace Src\Controllers;

use Exception;
use JetBrains\PhpStorm\NoReturn;
use Src\Classes\Request;
use Src\Classes\View;

/**
 * Class Request
 * @author Martin Osusky
 */
class Records
{
    /**
     * Default DNS properties
     */
    const DEFAULT_PROPERTIES = [
        'name'    => 'text',
        'content' => 'text',
        'ttl'     => 'number',
    ];

    /**
     * Define all DNS record types and its properties
     * @var string[][]
     */
    const RECORD_TYPES = [
        'A'     => self::DEFAULT_PROPERTIES,
        'AAAA'  => self::DEFAULT_PROPERTIES,
        'MX'    => [
            'name'    => 'text',
            'content' => 'text',
            'prio'    => 'number',
            'ttl'     => 'number',
        ],
        'ANAME' => self::DEFAULT_PROPERTIES,
        'CNAME' => self::DEFAULT_PROPERTIES,
        'NS'    => self::DEFAULT_PROPERTIES,
        'TXT'   => self::DEFAULT_PROPERTIES,
        'SRV'   => [
            'name'    => 'text',
            'content' => 'text',
            'prio'    => 'number',
            'port'    => 'number',
            'weight'  => 'number',
            'ttl'     => 'number',
        ],
    ];

    public function __construct(public Request $request, public View $view, public string $userId)
    {
        // DNS Records Controller Init
    }

    /**
     * @return array|string|null
     * @throws Exception
     */
    protected function getDomainName(): array|string|null
    {
        if (!$domainName = param('domainName')) {
            throw new Exception("Domain name not found.");
        }
        return $domainName;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function index(): string
    {
        $domainName = $this->getDomainName();
        $records = $this->request->get("user/$this->userId/zone/$domainName/record")->response->items;

        return $this->view->render('records/list', compact('records', 'domainName'));
    }

    /**
     * (new / existing) DNS record form
     * @throws Exception
     */
    public function edit(): string
    {
        $domainName = $this->getDomainName();

        if ($recordId = param('recordId')) {
            // edit / update - load existing DNS record
            $data = (array)$this->request->get("user/$this->userId/zone/$domainName/record/$recordId")->response;
        } else {
            // create new DNS record
            $data = [];
        }

        $errors = [];
        if (param('status') == 'error' and $response = @$_SESSION['response']) {
            unset($_SESSION['response']);
            session_write_close();

            // overwrite with old form data in the case of invalid input user data
            $data = array_merge($data, (array)$response->item);

            // prepare input error messages
            $errors = (array)$response->errors;
        }

        $newRecord = !(bool)@$data['id'];

        return $this->view->render('records/edit', compact(
            'data',
            'domainName',
            'errors',
            'newRecord',
        ));
    }

    /**
     * Process user data and save new or updated existing DNS record
     * @throws Exception
     */
    #[NoReturn] public function store()
    {
        // data preparation
        $recordType = param('type');
        $domainName = $this->getDomainName();
        $data = [];
        foreach (self::RECORD_TYPES[$recordType] as $name => $value) {
            $data[$name] = param($name);
        }
        $data['type'] = $recordType;

        if ($recordId = param('recordId')) {
            // update existing DNS record
            $url = "user/$this->userId/zone/$domainName/record/$recordId";
            $result = $this->request->put($url, $data);
        } else {
            // create new DNS record
            $url = "user/$this->userId/zone/$domainName/record";
            $result = $this->request->post($url, $data);
        }

        // result processing
        if ($result?->response?->status == 'success') {
            // code: 200, 201
            // success
            // redirect back to DNS records list with OK message
            redirect("/record/list?domainName=$domainName&message=saved_successful");

        } elseif ($result?->response?->status == 'error') {
            // code: 400
            // some data issue
            if ($result?->response?->errors) {
                // save invalid data to session for user correction in next step
                $_SESSION['response'] = $result->response;
            }
            session_write_close();

            // redirect back to form for data correction
            redirect("/record/edit?domainName=$domainName&recordId=$recordId&status=error");

        } else {
            // code 404
            // unexpected error
            return $this->view->render("errors/general", ['exception' => $result]);
        }
    }

    /**
     * Delete DNS record
     * @throws Exception
     */
    public function delete()
    {
        // data preparation
        $domainName = $this->getDomainName();

        if (!$recordId = param('recordId')) {
            throw new Exception("Record ID missing.");
        }

        // make DELETE API request
        $url = "user/$this->userId/zone/$domainName/record/$recordId";
        $result = $this->request->delete($url);

        // result processing
        if ($result?->response?->status == 'success') {
            // success
            // redirect back to DNS records list with OK message
            redirect("/record/list?domainName=$domainName&message=deleting_successful");
        } else {
            // unexpected error
            return $this->view->render("errors/general", ['exception' => 'result']);
        }
    }
}