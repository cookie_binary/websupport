<?php

namespace Src\Controllers;

use Exception;
use Src\Classes\Request;
use Src\Classes\View;

/**
 * Class Request
 * @author Martin Osusky
 */
class Domains
{
    public function __construct(public Request $request, public View $view, public string $userId)
    {
        // Domains Controller Init
    }

    public function index(): string
    {
        // get user domains
        $domains = $this->request->get("user/$this->userId/zone")->response->items;

        // render page
        return $this->view->render('domains/list', compact('domains'));
    }
}