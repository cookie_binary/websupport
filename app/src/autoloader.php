<?php

/**
 * Simple autoloader, so we don't need Composer just for this.
 * PSR0 implementation of Autoloading Standard
 * More info: https://www.php-fig.org/psr/psr-0/
 * @author Martin Osusky
 */
class Autoloader
{
    public static function register()
    {
        spl_autoload_register(function ($class) {

            $file =  "../" . strtolower(str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php');

            if (file_exists($file)) {
                require_once $file;
                return true;
            }
            return false;
        });
    }
}

Autoloader::register();
