<?php
/**
 * Returns array of possible routes
 *
 * @author Martin Osusky
 */

return [
    '/'              => 'Domains@index',
    '/record/list'   => 'Records@index',
    '/record'        => 'Records@detail',
    '/record/edit'   => 'Records@edit',
    '/record/store'  => 'Records@store',
    '/record/delete' => 'Records@delete',
];
