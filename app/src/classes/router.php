<?php

namespace Src\Classes;

use Exception;

/**
 * Class Request
 * @author Martin Osusky
 */
class Router
{
    public function __construct(public array $routes, public Request $request, public View $view)
    {
        // Router init
    }

    /**
     * Resolve controller calls
     *
     * @throws Exception
     */
    public function resolve()
    {
        $request = parse_url($_SERVER['REQUEST_URI'])['path'];

        if (!$action = @$this->routes[$request]) {
            throw new Exception("Route not found. Requested URI: `$request`");
        }

        // Prepare controller
        list($controller, $callable) = explode(separator: "@", string: $action);
        $className = "Src\\Controllers\\$controller";
        $instance = new $className($this->request, $this->view, getenv('USER_ID'));

        // Call action
        return call_user_func([$instance, $callable]);
    }
}