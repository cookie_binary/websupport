<?php

namespace Src\Classes;
/**
 * Class Request
 * @author Martin Osusky
 */
class Request
{
    /**
     * Request constructor.
     * @param string $apiUrl
     * @param string $apiKey
     * @param string $apiSecret
     * @param string $apiVersion
     */
    public function __construct(public string $apiUrl, public string $apiKey, public string $apiSecret, public string $apiVersion)
    {
        // setup Request class
    }

    /**
     * Returns requested parameter
     * @param $paramName
     * @return string|array|null
     */
    public function param($paramName): string|array|null
    {
        return @$_POST[$paramName] ?? @$_GET[$paramName];
    }

    /**
     * Make GET method HTTP request
     * @param $action
     * @return object
     */
    public function get($action): object
    {
        return $this->call("GET", "/$this->apiVersion/$action", "");
    }

    /**
     * Make POST method HTTP request
     * @param string $action
     * @param array $data
     * @return object|bool|string
     */
    public function post(string $action, array $data): object|bool|string
    {
        return $this->call("POST", "/$this->apiVersion/$action", $data);
    }

    /**
     * Make PUT method HTTP request
     * @param string $action
     * @param array $data
     * @return object|bool|string
     */
    public function put(string $action, array $data): object|bool|string
    {
        return $this->call("PUT", "/$this->apiVersion/$action", $data);
    }

    /**
     * Make DELETE method HTTP request
     * @param string $action
     * @return object|bool|string
     */
    public function delete(string $action): object|bool|string
    {
        return $this->call("DELETE", "/$this->apiVersion/$action");
    }

    /**
     * Common call HTTP request
     *
     * @param string $method
     * @param string $path
     * @param array|string|null $data
     * @return object
     */
    private function call(string $method, string $path, array|string $data = null): object
    {
        $postPut = in_array($method, ['POST', 'PUT']);
        $time = time();
        $query = ''; // query part is optional and may be empty

        $canonicalRequest = sprintf('%s %s %s', $method, $path, $time);
        $signature = hash_hmac('sha1', $canonicalRequest, $this->apiSecret);

        $headers = ['Date: ' . gmdate('Ymd\THis\Z', $time),];
        $postPut && $headers[] = 'Content-Type: application/json';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url = sprintf('%s%s%s', $this->apiUrl, $path, $query));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $this->apiKey . ':' . $signature);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        $postPut && curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        return (object)[
            'response'  => json_decode($response),
            'http_code' => $info['http_code'],
        ];
    }
}
