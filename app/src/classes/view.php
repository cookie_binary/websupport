<?php

namespace Src\Classes;

use Exception;

/**
 * Class View
 * @author Martin Osusky
 */
class View
{
    public function __construct(protected string $path, public string $layout)
    {
        // View init
    }

    /**
     * Render content
     *
     * @param string $view
     * @param array $data
     * @return mixed
     */
    public function renderContent(string $view, array $data = []): string
    {
        ob_start();
        extract($data);
        include "/app/" . $this->path . $view . ".html.php";
        return ob_get_clean();
    }

    /**
     * Resolve controller calls
     *
     * @param string $view
     * @param array $data
     * @return string
     */
    public function render(string $view, array $data = []): string
    {
        // render content of page content
        $content = $this->renderContent($view, $data);

        // render layout with content as parameter
        return $this->renderContent($this->layout, compact('content'));
    }
}